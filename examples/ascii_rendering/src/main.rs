
// Rende sub-modules
use rende_rs::{
    badmap::Bitmap,
    geo::{v, Mat3, Vector3, Vertex},
    obj_3d::Obj, 
    Texture, FragmentShader, Framebuffer, Renderer, VertexShader,
};

use console::Term;
use std::io::Write;
use std::path::Path;
use std::time::Duration;

const RENDER_WIDTH: usize = 1280;
const RENDER_HEIGHT: usize = 720;
static ASCII_SHADES: &str = " ..--===+++%%%@@@####";

struct Uniforms {
    translation: Vector3,
    rotation: Mat3,
    scale: Vector3,
    texture: Texture,
    light_dir: Vector3,
}

struct VS{}
struct FS{}
impl VertexShader<Uniforms> for VS {
    fn apply(&self, src_vertex: &Vertex, uniforms: &Uniforms) -> Vertex {
        let mut vert = *src_vertex;

        // Scale
        vert.position.x *= uniforms.scale.x;
        vert.position.y *= uniforms.scale.y;
        vert.position.z *= uniforms.scale.z;

        // Rotate
        vert.position = uniforms.rotation * vert.position;
        vert.normal = uniforms.rotation * vert.normal;

        // Translate
        vert.position = uniforms.translation + vert.position;

        vert
    }
}

impl FragmentShader<u8, Uniforms> for FS {
    fn apply(&self, vertex: Vertex, _frag: &u8, uniforms: &Uniforms) -> u8 {
        let dot = Vector3::dot(vertex.normal, uniforms.light_dir);
        let intensity = f32::min(1.0, f32::max(0.0, dot));

        let tex_col = uniforms
            .texture
            .uv_sample(vertex.tex_coord.x, vertex.tex_coord.y);
        let r = (tex_col.r as f32 * intensity) / 255.0;
        let g = (tex_col.g as f32 * intensity) / 255.0;
        let b = (tex_col.b as f32 * intensity) / 255.0;
        let average = f32::min(1.0, (r+g+b) / 3.0);

        let shade = average * ASCII_SHADES.len() as f32;
        ASCII_SHADES.as_bytes()[shade as usize]
    }
}

fn main() {
    let mut byte_buf = [0u8; 1];
    char::encode_utf8(' ', &mut byte_buf);
    let space = byte_buf[0];

    let mut term = Term::stdout();
    term.hide_cursor().unwrap();
    let mut framebuffer = Framebuffer::new(RENDER_WIDTH, RENDER_HEIGHT, 0.1, 1000.0, space);

    let bmp = Bitmap::load(Path::new("assets/stone.bmp")).unwrap();
    let tex = Texture::from_bitmap(bmp, None);
    let light_direction = v(0.0, 0.0, 1.0);

    let vshader = VS{};
    let fshader = FS{};
    let mut renderer = Renderer::new(&vshader, &fshader);

    // Resolve model triangles from OBJ
    let obj = Obj::load(std::path::Path::new("./assets/GossipStone.obj")).unwrap();
    let mut triangles = Vec::with_capacity(obj.faces().len());
    for triangle in obj.triangles() {
        triangles.push(triangle);
    }

    let rot_step = 0.1;
    let mut y_rot = 0.0;
    let mut uniforms = Uniforms {
        translation: v(30.0, 23.0, -100.0), 
        rotation: Mat3::y_rotation(y_rot),

        // Young Link
        //scale: v(1.0, -0.5, 1.0)/2.5,

        // Gossip Stone
        scale: v(1.0, -0.5, 1.0),

        light_dir: light_direction,
        texture: tex,
    };

    loop {
        // Render to framebuffer
        framebuffer.clear(space);
        renderer.render_triangles(&mut framebuffer, &triangles, &uniforms);

        // Update rotation
        uniforms.rotation = Mat3::y_rotation(y_rot);
        y_rot += rot_step;

        // Render to terminal
        let term_size = term.size();
        let x_lim = usize::min(term_size.1 as usize, RENDER_WIDTH);

        term.clear_screen().unwrap();
        for y in 0..term_size.0 as usize {
            term.move_cursor_to(0, y).unwrap();
            let row_idx = y * RENDER_WIDTH;
            let row_end = row_idx + x_lim;
            term.write(&framebuffer.data_buf[row_idx..row_end]).unwrap();
        }

        std::thread::sleep(Duration::from_millis(100));
    }
}
