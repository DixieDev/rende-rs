use crate::{Fragment, UniformData};
use rende_rs::geo::{Vector3, Vertex};
use rende_rs::colour::{rgba, Colour};
use rende_rs::{FragmentShader, Framebuffer, VertexShader};

/// Vertex shader that just spits back a copy of the provided vertex
pub struct IdentityVertShader {}

impl VertexShader<Framebuffer<Fragment>> for IdentityVertShader {
    fn apply(&self, src_vertex: &Vertex, _framebuffer: &Framebuffer<Fragment>) -> Vertex {
        *src_vertex
    }
}

/// Basic vertex shader, performs transformation from model space to world space
pub struct BasicVertShader {}

impl<'a> VertexShader<UniformData<'a>> for BasicVertShader {
    fn apply(&self, src_vertex: &Vertex, uniforms: &UniformData<'a>) -> Vertex {
        let mut vert = *src_vertex;

        // Scale
        vert.position.x *= uniforms.scale;
        vert.position.y *= uniforms.scale;
        vert.position.z *= uniforms.scale;

        // Rotate
        vert.position = uniforms.rotation * vert.position;
        vert.normal = uniforms.rotation * vert.normal;

        // Translate
        vert.position = uniforms.translation + vert.position;

        vert
    }
}

/// Fragment shader that outputs shaded colours as well as normal data
pub struct ColNormFragShader {}

impl<'a> FragmentShader<Fragment, UniformData<'a>> for ColNormFragShader {
    fn apply(
        &self,
        frag_vertex: Vertex,
        _cur_frag: &Fragment,
        uniforms: &UniformData<'a>,
    ) -> Fragment {
        // Determine intensity of colour based on dot product between normal and
        // the global light direction
        let dot = Vector3::dot(frag_vertex.normal, uniforms.light_dir);
        let intensity = f32::min(1.0, f32::max(0.0, dot));

        // Modify RGB colour values based on light intensity, but retain alpha value
        let tex_col = uniforms
            .texture
            .uv_sample(frag_vertex.tex_coord.x, frag_vertex.tex_coord.y);
        let r = (tex_col.r as f32 * intensity) as u8;
        let g = (tex_col.g as f32 * intensity) as u8;
        let b = (tex_col.b as f32 * intensity) as u8;

        Fragment {
            colour: rgba(r, g, b, tex_col.a),
            normal: frag_vertex.normal,
        }
    }
}

/// Draws outlines over fragments with significantly different normal values to the surrounding fragments
pub struct OutlineFragShader {}

impl FragmentShader<Colour, Framebuffer<Fragment>> for OutlineFragShader {
    fn apply(
        &self,
        frag_vertex: Vertex,
        _cur_frag: &Colour,
        src: &Framebuffer<Fragment>,
    ) -> Colour {
        let threshold = 3.0;

        let x = frag_vertex.position.x as usize;
        let y = frag_vertex.position.y as usize;

        let left_x = if x == 0 { x } else { x - 1 };
        let right_x = if x < src.width() - 2 {
            x + 1
        } else {
            src.width() - 1
        };
        let down_y = if y == 0 { y } else { y - 1 };
        let up_y = if y < src.height() - 2 {
            y + 1
        } else {
            src.height() - 1
        };

        let cur_frag = src.data_buf[x + y * src.width()].clone();
        let left_frag = src.data_buf[left_x + y * src.width()].clone();
        let right_frag = src.data_buf[right_x + y * src.width()].clone();
        let down_frag = src.data_buf[x + down_y * src.width()].clone();
        let up_frag = src.data_buf[x + up_y * src.width()].clone();

        let left_diff = Vector3::dot(cur_frag.normal, left_frag.normal);
        let right_diff = Vector3::dot(cur_frag.normal, right_frag.normal);
        let down_diff = Vector3::dot(cur_frag.normal, down_frag.normal);
        let up_diff = Vector3::dot(cur_frag.normal, up_frag.normal);
        let diff = left_diff + right_diff + down_diff + up_diff;

        if diff <= threshold && cur_frag.colour.a > 0 {
            rgba(255, 0, 0, cur_frag.colour.a)
        } else {
            cur_frag.colour
        }
    }
}
