mod input;
mod shaders;

use crate::input::InputState;
use crate::shaders::{ BasicVertShader, ColNormFragShader, IdentityVertShader, OutlineFragShader };
use rende_rs::{
    badmap::Bitmap,
    geo::{v, Mat3, Triangle, Vector2, Vector3, Vertex},
    obj_3d::Obj,
    colour::{rgb, rgba, Colour},
    Framebuffer, Renderer, Texture, 
};
use sdl2::{pixels::PixelFormatEnum, render::TextureAccess};
use std::path::Path;
use std::time::{Duration, Instant};

const RENDER_WIDTH: usize = 480;
const RENDER_HEIGHT: usize = 320;
const FRAME_RATE: u64 = 30;

pub struct UniformData<'a> {
    pub scale: f32,
    pub rotation: Mat3,
    pub translation: Vector3,
    pub light_dir: Vector3,
    pub texture: &'a Texture,
}

#[derive(Clone)]
pub struct Fragment {
    pub colour: Colour,
    pub normal: Vector3,
}

impl Default for Fragment {
    fn default() -> Self {
        Fragment {
            colour: rgba(0, 0, 0, 0),
            normal: Vector3::new(0.0, 0.0, 0.0),
        }
    }
}

fn update_texture_with_framebuffer(
    texture: &mut sdl2::render::Texture,
    framebuffer: &Framebuffer<Colour>,
) {
    let pixels: Vec<u8> = framebuffer
        .data_buf
        .iter()
        .flat_map(|c| vec![c.r, c.g, c.b, c.a])
        .collect();

    texture
        .update(None, &pixels, framebuffer.width() * 4)
        .unwrap();
}

fn main() -> Result<(), String> {
    let bmp = Bitmap::load(Path::new("assets/stone.bmp")).unwrap();
    let tex = Texture::from_bitmap(bmp, None);

    // Open SDL window and create canvas
    let sdl = sdl2::init().unwrap();
    let video_subsystem = sdl.video().unwrap();

    let window = video_subsystem
        .window("rende-rs", RENDER_WIDTH as u32, RENDER_HEIGHT as u32)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().software().build().unwrap();
    canvas.set_draw_color(sdl2::pixels::Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();

    // Create texture that will be used to render to the canvas
    let texture_creator = canvas.texture_creator();
    let mut texture = texture_creator
        .create_texture(
            Some(PixelFormatEnum::ABGR8888),
            TextureAccess::Streaming,
            RENDER_WIDTH as u32,
            RENDER_HEIGHT as u32,
        )
        .unwrap();

    // Directional light
    let light_direction = v(0.0, 0.0, 1.0);

    let vertex_shader = BasicVertShader {};
    let col_norm_frag_shader = ColNormFragShader {};
    let outline_vert_shader = IdentityVertShader {};

    // Second pass frag shader checks diff between normals and draw outline if they're sufficiently different
    let outline_frag_shader = OutlineFragShader {};

    // Create framebuffer and renderer
    let mut framebuffer = Framebuffer::new(
        RENDER_WIDTH,
        RENDER_HEIGHT,
        0.1,
        1000.0,
        Fragment::default(),
    );
    let mut screen_buffer =
        Framebuffer::new(RENDER_WIDTH, RENDER_HEIGHT, 0.0, 10.0, rgb(0, 0, 0));
    let mut first_pass_renderer = Renderer::new(&vertex_shader, &col_norm_frag_shader);
    let mut second_pass_renderer =
        Renderer::new(&outline_vert_shader, &outline_frag_shader);

    // Load OBJ file
    let obj = Obj::load(std::path::Path::new("./assets/GossipStone.obj")).unwrap();

    // Resolve model triangles from OBJ
    let mut triangles = Vec::with_capacity(obj.faces().len());
    for triangle in obj.triangles() {
        triangles.push(triangle);
    }

    // Free OBJ data
    drop(obj);

    // Create triangles for simple plane
    let plane_verts = [
        Vertex::new(
            Vector3::new(0.0, RENDER_HEIGHT as f32, -1.0),
            Vector2::new(0.0, 0.0),
            Vector3::new(0.0, 0.0, 1.0),
        ),
        Vertex::new(
            Vector3::new(RENDER_WIDTH as f32, RENDER_HEIGHT as f32, -1.0),
            Vector2::new(0.0, 0.0),
            Vector3::new(0.0, 0.0, 1.0),
        ),
        Vertex::new(
            Vector3::new(RENDER_WIDTH as f32, 0.0, -1.0),
            Vector2::new(0.0, 0.0),
            Vector3::new(0.0, 0.0, 1.0),
        ),
        Vertex::new(
            Vector3::new(0.0, 0.0, -1.0),
            Vector2::new(0.0, 0.0),
            Vector3::new(0.0, 0.0, 1.0),
        ),
    ];

    let plane = [
        Triangle::new([plane_verts[0], plane_verts[1], plane_verts[2]]),
        Triangle::new([plane_verts[2], plane_verts[3], plane_verts[0]]),
    ];

    // Establish some variables for the models transform
    // TODO: Make a Transform struct with nice API
    let mut x_rot = 0.0;
    let mut y_rot = 0.0;
    let rot_rps = 0.2;
    let x_rot_step = 2.0 * std::f32::consts::PI * rot_rps;
    let y_rot_step = 2.0 * std::f32::consts::PI * rot_rps;

    let scale = 5.0;
    let x_off = framebuffer.width() as i64 / 2;
    let y_off = 20;

    let z_off = -500.0;

    // Start the tick loop
    let mut event_pump = sdl.event_pump().unwrap();
    let mut running = true;
    let mut now = Instant::now();
    let tick_interval = Duration::from_millis(1000 / FRAME_RATE);

    let mut input_state = InputState::new();
    while running {
        // Update delta-time value
        let dt = now.elapsed().as_millis() as f32 / 1000.0;
        now = Instant::now();

        // Handle events
        input_state.update();
        for event in event_pump.poll_iter() {
            input_state.handle_event(&event);
        }
        if input_state.escape_pressed() || input_state.quit_pressed() {
            running = false;
        }

        // Update model transform based on input
        y_rot += y_rot_step * dt * input_state.x_axis();
        x_rot += x_rot_step * dt * input_state.y_axis();

        // Render model to framebuffer
        framebuffer.clear(Fragment::default());
        screen_buffer.clear(rgb(0, 0, 0));

        let uniforms = UniformData {
            scale,
            rotation: Mat3::x_rotation(x_rot) * Mat3::y_rotation(y_rot),
            translation: [x_off as f32, y_off as f32, z_off].into(),
            light_dir: light_direction,
            texture: &tex,
        };
        first_pass_renderer.render_triangles(
            &mut framebuffer,
            &triangles,
            &uniforms,
        );
        second_pass_renderer.render_triangles(
            &mut screen_buffer,
            &plane,
            &framebuffer,
        );

        // Copy framebuffer data to window canvas
        canvas.clear();
        update_texture_with_framebuffer(&mut texture, &screen_buffer);
        canvas
            .copy_ex(&texture, None, None, 0.0, None, false, true)
            .unwrap();

        // Sleep as long as necessary to hit the minimum frame duration before presenting
        let elapsed = now.elapsed();
        if elapsed < tick_interval {
            std::thread::sleep(tick_interval - now.elapsed());
        }
        canvas.present();
    }

    Ok(())
}
