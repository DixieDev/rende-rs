use sdl2::{event::Event, keyboard::Keycode};

pub struct InputState {
    quit_pressed: bool,
    escape_pressed: bool,
    up_held: bool,
    right_held: bool,
    down_held: bool,
    left_held: bool,
}

impl InputState {
    pub fn new() -> Self {
        Self {
            quit_pressed: false,
            escape_pressed: false,
            up_held: false,
            right_held: false,
            down_held: false,
            left_held: false,
        }
    }

    pub fn quit_pressed(&self) -> bool {
        self.quit_pressed
    }

    pub fn escape_pressed(&self) -> bool {
        self.escape_pressed
    }

    pub fn x_axis(&self) -> f32 {
        let mut axis = 0.0;
        if self.left_held {
            axis -= 1.0;
        }
        if self.right_held {
            axis += 1.0;
        }
        axis
    }

    pub fn y_axis(&self) -> f32 {
        let mut axis = 0.0;
        if self.up_held {
            axis += 1.0;
        }
        if self.down_held {
            axis -= 1.0;
        }
        axis
    }

    pub fn update(&mut self) {
        self.quit_pressed = false;
        self.escape_pressed = false;
    }

    pub fn handle_event(&mut self, event: &Event) {
        match event {
            Event::Quit { .. } => self.quit_pressed = true,
            Event::KeyDown {
                keycode: Some(key), ..
            } => self.handle_key_press(key),
            Event::KeyUp {
                keycode: Some(key), ..
            } => self.handle_key_release(key),
            _ => (),
        }
    }

    fn handle_key_press(&mut self, key: &Keycode) {
        match key {
            Keycode::Escape => self.escape_pressed = true,
            Keycode::Up => self.up_held = true,
            Keycode::Right => self.right_held = true,
            Keycode::Down => self.down_held = true,
            Keycode::Left => self.left_held = true,
            _ => (),
        }
    }

    fn handle_key_release(&mut self, key: &Keycode) {
        match key {
            Keycode::Up => self.up_held = false,
            Keycode::Right => self.right_held = false,
            Keycode::Down => self.down_held = false,
            Keycode::Left => self.left_held = false,
            _ => (),
        }
    }
}
