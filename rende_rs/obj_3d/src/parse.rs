
pub struct ParseResult<'s, T> {
    pub value: Option<T>,
    pub leftovers: &'s str,
}

pub fn slice_or_empty(source: &str, start: usize) -> &str {
    if start < source.len() {
        &source[start..]
    } else {
        ""
    }
}

fn erase_whitespace(source: &str) -> &str {
    for (i, c) in source.chars().enumerate() {
        if !c.is_whitespace() {
            return &source[i..];
        }
    }
    ""
}

pub fn expect_string<'s>(source: &'s str, expected: &str) -> Result<&'s str, &'s str> {
    let source = erase_whitespace(source);
    if source.starts_with(expected) {
        Ok(slice_or_empty(source, expected.len()))
    } else {
        Err(source)
    }
}

pub fn consume_usize<'s>(source: &'s str) -> ParseResult<'s, usize> {
    let source = erase_whitespace(source);
    let mut buf = String::new();
    let mut last_i = 0;
    for (i, c) in source.chars().enumerate() {
        last_i = i;
        if c.is_numeric() {
            buf.push(c);
        } else if !buf.is_empty() {
            return ParseResult {
                value: Some(buf.parse().unwrap()),
                leftovers: slice_or_empty(source, i),
            };
        }
    }
    if !buf.is_empty() {
        return ParseResult {
            value: Some(buf.parse().unwrap()),
            leftovers: slice_or_empty(source, last_i + 1),
        };
    }

    ParseResult {
        value: None,
        leftovers: source,
    }
}

pub fn consume_f32<'s>(source: &'s str) -> ParseResult<'s, f32> {
    let source = erase_whitespace(source);
    let mut last_i = 0;
    let mut buf = String::new();
    for (i, c) in source.chars().enumerate() {
        last_i = i;
        if c.is_numeric() || c == '.' || c == '-' {
            buf.push(c);
        } else if !buf.is_empty() {
            return ParseResult {
                value: buf.parse().ok(),
                leftovers: slice_or_empty(source, i),
            };
        }
    }
    if !buf.is_empty() {
        return ParseResult {
            value: buf.parse().ok(),
            leftovers: slice_or_empty(source, last_i + 1),
        };
    }

    ParseResult {
        value: None,
        leftovers: source,
    }
}