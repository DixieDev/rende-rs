mod parse;

use crate::parse::{consume_f32, consume_usize, expect_string};
use geo::{Triangle, Vector2, Vector3, Vertex};
use std::{
    convert::From,
    fs::File,
    io::{prelude::*, BufReader},
    iter::Iterator,
    path::Path,
};

pub struct ObjTriangles<'o> {
    obj: &'o Obj,
    face_idx: usize,
    face_count: usize,
}

impl <'o> Iterator for ObjTriangles<'o> {
    type Item = Triangle;

    fn next(&mut self) -> Option<Self::Item> {
        if self.face_idx == self.face_count {
            None
        } else {
            let face = &self.obj.faces[self.face_idx];
            self.face_idx += 1;
            let positions = [
                self.obj.verts[face.vert_indices[0]],
                self.obj.verts[face.vert_indices[1]],
                self.obj.verts[face.vert_indices[2]],
            ];
            let normals = match face.normal_indices.len() {
                3 => [ 
                    self.obj.normals[face.normal_indices[0]],
                    self.obj.normals[face.normal_indices[1]],
                    self.obj.normals[face.normal_indices[2]],
                ],
                _ => [
                    positions[0].normalized(),
                    positions[1].normalized(),
                    positions[2].normalized(),
                ]
            };
            let tex_coords = match face.tex_coord_indices.len() {
                3 => [
                    self.obj.tex_coords[face.tex_coord_indices[0]],
                    self.obj.tex_coords[face.tex_coord_indices[1]],
                    self.obj.tex_coords[face.tex_coord_indices[2]],
                ],
                _ => [
                    Vector2::new(0.0, 0.0),
                    Vector2::new(1.0, 0.0),
                    Vector2::new(1.0, 1.0),
                ]
            };

            let vertices = [
                Vertex::new(positions[0], tex_coords[0], normals[0]),
                Vertex::new(positions[1], tex_coords[1], normals[1]),
                Vertex::new(positions[2], tex_coords[2], normals[2]),
            ];
            let tri = Triangle::new(vertices);
            Some(tri)
        }
    }
}

pub struct Face {
    pub vert_indices: Vec<usize>,
    pub tex_coord_indices: Vec<usize>,
    pub normal_indices: Vec<usize>,
}

pub struct Obj {
    pub faces: Vec<Face>,
    pub verts: Vec<Vector3>,
    pub tex_coords: Vec<Vector2>,
    pub normals: Vec<Vector3>,
}

impl Obj {
    /// Return an iterator over each triangle defined by this Obj file
    pub fn triangles<'o>(&'o self) -> ObjTriangles<'o> {
        ObjTriangles {
            obj: self,
            face_count: self.faces.len(),
            face_idx: 0,
        }
    }

    /// Load an OBJ from a file
    pub fn load(path: &Path) -> Result<Self, String> {
        let f = File::open(path).map_err(|e| e.to_string())?;
        let reader = BufReader::new(f);

        let mut faces = Vec::new();
        let mut verts = Vec::new();
        let mut tex_coords = Vec::new();
        let mut normals = Vec::new();

        let mut buf = String::new();
        for (line_number, line) in reader.lines().enumerate() {
            let line_number = line_number + 1;
            let line = line.map_err(|e| e.to_string())?;
            if let Ok(line) = expect_string(&line, "v ") {
                // Vertex
                let x_result = consume_f32(line);
                let y_result = consume_f32(x_result.leftovers);
                let z_result = consume_f32(y_result.leftovers);
                if let (Some(x), Some(y), Some(z)) =
                    (x_result.value, y_result.value, z_result.value)
                {
                    verts.push(Vector3 { x, y, z });
                } else {
                    return Err(String::from(format!(
                        "Failed to read vertex values on line {}",
                        line_number
                    )));
                }
            } else if let Ok(line) = expect_string(&line, "vt ") {
                // Texture coord
                let u_result = consume_f32(line);
                let v_result = consume_f32(u_result.leftovers);
                if let (Some(u), Some(v)) = (u_result.value, v_result.value) {
                    tex_coords.push(Vector2 { x: u, y: v });
                } else {
                    return Err(String::from(format!(
                        "Failed to read texture coordinates on line {}",
                        line_number
                    )));
                }
            } else if let Ok(line) = expect_string(&line, "vn ") {
                // Normal
                let x_result = consume_f32(line);
                let y_result = consume_f32(x_result.leftovers);
                let z_result = consume_f32(y_result.leftovers);
                if let (Some(x), Some(y), Some(z)) =
                    (x_result.value, y_result.value, z_result.value)
                {
                    normals.push(Vector3 { x, y, z });
                } else {
                    return Err(String::from(format!(
                        "Failed to read vertex values on line {}",
                        line_number
                    )));
                }
            } else if let Ok(mut line) = expect_string(&line, "f ") {
                // Face
                // Determine number of slashes to figure out format
                let slash_count = line.chars().filter(|c| *c == '/').count();

                // Read up to 9 usize values from the current line
                let mut values = Vec::new();
                for _ in 0..9 {
                    let result = consume_usize(line);
                    line = result.leftovers;
                    match result.value {
                        Some(v) => values.push(v-1),
                        None => break,
                    }
                }

                // Read vec of values based on number of slashes
                match slash_count {
                    0 => {
                        // No slashes means only vertex positions are defined
                        match values.len() {
                            3 => faces.push(Face {
                                vert_indices: values,
                                tex_coord_indices: Vec::new(),
                                normal_indices: Vec::new(),
                            }),
                            n => return Err(String::from(format!(
                                "Unexpected number of vertices for face defined on line {}. Expected 3 but found {}.",
                                line_number, n
                            ))),
                        }
                    }
                    3 => {
                        // One slash per vertex means vertex positions and texture coords are defined
                        match values.len() {
                            6 => faces.push(Face {
                                vert_indices: vec![values[0], values[2], values[4]],
                                tex_coord_indices: vec![values[1], values[3], values[5]],
                                normal_indices: Vec::new(),
                            }),
                            n => return Err(String::from(format!(
                                "Unexpected number of attributes for face defined on line {}. Expected 6 but found {}.",
                                line_number, n
                            ))),
                        }
                    }
                    6 => {
                        // Can either have 2 slashes and 2 values (vertex and normal),
                        // or 2 slashes and 3 values (vertex, tex coord, and normal)
                        match values.len() {
                            6 => faces.push(Face {
                                vert_indices: vec![values[0], values[2], values[4]],
                                tex_coord_indices: Vec::new(),
                                normal_indices: vec![values[1], values[3], values[5]],
                            }),
                            9 => faces.push(Face {
                                vert_indices: vec![values[0], values[3], values[6]],
                                tex_coord_indices: vec![values[1], values[4], values[7]],
                                normal_indices: vec![values[2], values[5], values[8]],
                            }),
                            n => return Err(String::from(format!(
                                "Unexpected number of attributes for face defined on line {}. Expected 6 or 9 but found {}",
                                line_number, n
                            ))),
                        }
                    }
                    _ => {
                        return Err(String::from(format!(
                            "Too many attributes per vertex for face on line {}.",
                            line_number
                        )))
                    }
                }
            }
            buf.clear();
        }

        Ok(Self {
            faces,
            verts,
            normals,
            tex_coords,
        })
    }

    /// Get a reference to the internal vec of defined faces
    pub fn faces(&self) -> &[Face] {
        &self.faces
    }
}
