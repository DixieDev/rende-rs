use bencher::{benchmark_group, benchmark_main, Bencher};
use rende_rs::{
    geo::{ Vertex, Vector2, Vector3, Triangle },
    colour::Colour,
    Framebuffer,
    FragmentShader,
    VertexShader,
    Renderer,
};

struct VS{}

impl VertexShader<()> for VS {
    fn apply(&self, vert: &Vertex, _uniforms: &()) -> Vertex {
        *vert
    }
}

struct FS{}

impl FragmentShader<Colour, ()> for FS {
    fn apply(&self, _vert: Vertex, _frag: &Colour, _uniforms: &()) -> Colour {
        Colour::new(255, 255, 255, 255)
    }
}


fn render_triangle(b: &mut Bencher) {
    let mut fb = Framebuffer::new(1920, 1080, -10.0, 10.0, Colour::new(0, 0, 0, 0));
    let vs = VS{};
    let fs = FS{};
    let mut renderer = Renderer::new(&vs, &fs);

    let tris = [make_test_triangle()];
    
    b.iter(move || renderer.render_triangles(&mut fb, &tris, &()));
}

fn render_grid(b: &mut Bencher) {
    let mut fb = Framebuffer::new(1920, 1080, -10.0, 10.0, Colour::new(0, 0, 0, 0));
    let vs = VS{};
    let fs = FS{};
    let mut renderer = Renderer::new(&vs, &fs);

    let tris = make_test_mesh();
    
    b.iter(move || renderer.render_triangles(&mut fb, &tris, &()));
}

fn make_test_triangle() -> Triangle {
    Triangle::new([
        Vertex {
            position: Vector3::new(0.0, 1000.0, 0.0),
            tex_coord: Vector2::new(0.0, 0.0),
            normal: Vector3::new(0.0, 0.0, -1.0),
        },
        Vertex {
            position: Vector3::new(1000.0, 0.0, 0.0),
            tex_coord: Vector2::new(1.0, 1.0),
            normal: Vector3::new(0.0, 0.0, -1.0),
        },
        Vertex {
            position: Vector3::new(0.0, 0.0, 0.0),
            tex_coord: Vector2::new(0.0, 1.0),
            normal: Vector3::new(0.0, 0.0, -1.0),
        },
    ])
}

fn make_test_mesh() -> Vec<Triangle> {
    let min_x = 0.0;
    let max_x = 1920.0;
    let min_y = 0.0;
    let max_y = 1080.0;

    let grid_res_x = 400.0;
    let grid_res_y = 400.0;

    let num_tris = (grid_res_x * grid_res_y * 2.0) as usize;
    let mut tris = Vec::with_capacity(num_tris);

    let cell_w = (max_x - min_x) / grid_res_x;
    let cell_h = (max_y - min_y) / grid_res_y;

    for cell_y in 0..grid_res_y as usize {
        let bot = min_y + (cell_y as f32 * cell_h);
        let top = bot + cell_h;
        for cell_x in 0..grid_res_x as usize {
            let left = min_x + (cell_x as f32 * cell_w);
            let right = left + cell_w;

            tris.push(Triangle::new([
                Vertex {
                    position: Vector3::new(left, top, 0.0),
                    tex_coord: Vector2::new(0.0, 0.0),
                    normal: Vector3::new(0.0, 0.0, -1.0),
                },
                Vertex {
                    position: Vector3::new(right, top, 0.0),
                    tex_coord: Vector2::new(0.0, 0.0),
                    normal: Vector3::new(0.0, 0.0, -1.0),
                },
                Vertex {
                    position: Vector3::new(right, bot, 0.0),
                    tex_coord: Vector2::new(0.0, 0.0),
                    normal: Vector3::new(0.0, 0.0, -1.0),
                },
            ]));

            tris.push(Triangle::new([
                Vertex {
                    position: Vector3::new(left, top, 0.0),
                    tex_coord: Vector2::new(0.0, 0.0),
                    normal: Vector3::new(0.0, 0.0, -1.0),
                },
                Vertex {
                    position: Vector3::new(right, bot, 0.0),
                    tex_coord: Vector2::new(0.0, 0.0),
                    normal: Vector3::new(0.0, 0.0, -1.0),
                },
                Vertex {
                    position: Vector3::new(left, bot, 0.0),
                    tex_coord: Vector2::new(0.0, 0.0),
                    normal: Vector3::new(0.0, 0.0, -1.0),
                },
            ]));
        }
    }
    
    tris
}

benchmark_group!(
    benches,
    render_triangle,
    render_grid,
);

benchmark_main!(benches);
