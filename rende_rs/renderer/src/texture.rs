use crate::colour::{rgba, rgb, Colour};
use badmap::Bitmap;

pub struct Texture {
    pixels: Vec<Colour>,
    width: usize,
    height: usize,
}

impl Texture {
    // TODO better error handling
    pub fn from_bitmap(bmp: Bitmap, colour_key: Option<[u8; 3]>) -> Self {
        // Determine starting pixel coordinate
        let uwidth = i32::abs(bmp.width()) as usize;
        let uheight = i32::abs(bmp.height()) as usize;

        // Number of bytes per row should be rounded up to the nearest multiple of 4
        let pixel_bytes_per_row = uwidth * 3;
        let bytes_per_row = ((pixel_bytes_per_row + 3) / 4) * 4;

        // TODO Verify the starting points are correct for standard texture mapping coords
        let mut x = if bmp.width() >= 0 { 0 } else { uwidth - 1 };
        let mut y = if bmp.height() >= 0 { 0 } else { uheight - 1 };

        assert_eq!(bytes_per_row * uheight, bmp.bytes().len());
        let mut pixels = vec![rgba(0, 0, 0, 0); uwidth * uheight];
        for bmp_y in 0..uheight {
            for bmp_x in 0..uwidth {
                if bmp_x >= uwidth {
                    break;
                }
                let bmp_idx = bmp_x * 3 + bmp_y * bytes_per_row;
                let b = bmp.bytes()[bmp_idx + 0];
                let g = bmp.bytes()[bmp_idx + 1];
                let r = bmp.bytes()[bmp_idx + 2];

                // Assign new colour if not colour keyed
                if let Some(colour_key) = colour_key {
                    if r != colour_key[0] || g != colour_key[1] || b != colour_key[2] {
                        pixels[x + y * uwidth] = rgb(r, g, b);
                    }
                } else {
                    pixels[x + y * uwidth] = rgb(r, g, b);
                }

                if bmp.width() >= 0 { 
                    if x >= uwidth-1 {
                        if bmp.height() >= 0 {
                            y += 1;
                            x = 0;
                        } else if y != 0 {
                            y -= 1;
                            x = uwidth -1;
                        }
                    } else {
                        x += 1;
                    }
                } else { 
                    if x == 0 { 
                        if bmp.height() >= 0 {
                            y += 1;
                            x = 0;
                        } else if y != 0 {
                            y -= 1;
                            x = uwidth - 1;
                        }
                    } else {
                        x -= 1 
                    }
                }
            }
        }
        Texture {
            pixels,
            width: uwidth,
            height: uheight,
        }
    }

    pub fn uv_sample(&self, mut u: f32, mut v: f32) -> Colour {
        // Wrap U between -1.0 and 1.0
        u += 1.0;
        u = u - 2.0 * f32::floor(u / 2.0);
        u = f32::abs(u - 1.0);

        // Wrap V between -1.0 and 1.0
        v += 1.0;
        v = v - 2.0 * f32::floor(v / 2.0);
        v = f32::abs(v - 1.0);

        // Map wrapped UVs to a pixel on the texture
        let x = usize::min(self.width-1, ((self.width - 1) as f32 * u) as usize);
        let y = usize::min(self.height-1, ((self.height - 1) as f32 * v) as usize);

        // Return colour at mapped pixel's index
        self.pixels[x + y * self.width]
    }

    pub fn pixels(&self) -> &[Colour] {
        &self.pixels
    }

    pub fn width(&self) -> usize {
        self.width
    }

    pub fn height(&self) -> usize {
        self.height
    }
}