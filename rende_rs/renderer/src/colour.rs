/// A convenience function to create a `Colour` struct.
pub fn rgba(r: u8, g: u8, b: u8, a: u8) -> Colour {
    Colour::new(r, g, b, a)
}

/// A convenience function to create a `Colour` struct with a default alpha
/// value of 255.
pub fn rgb(r: u8, g: u8, b: u8) -> Colour {
    Colour::new(r, g, b, 255)
}

/// A 32-bit colour consisting of red, blue, green, and alpha components.
/// Can be converted to an array of four f32s via the `into()` implementation,
/// and doing so will normalise the component values. Likewise, an array of
/// four normalised f32s can be converted to a `Colour` struct.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Colour {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Colour {
    pub fn new(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self { r, g, b, a }
    }
}

impl From<[u8; 4]> for Colour {
    fn from(arr: [u8; 4]) -> Self {
        Self {
            r: arr[0],
            g: arr[1],
            b: arr[2],
            a: arr[3],
        }
    }
}

impl Into<[u8; 4]> for Colour {
    fn into(self) -> [u8; 4] {
        [self.r, self.g, self.b, self.a]
    }
}

impl From<[f32; 4]> for Colour {
    /// Converts the colour to an array of normalised floats.
    fn from(arr: [f32; 4]) -> Self {
        Self {
            r: (arr[0] * 255.0) as u8,
            g: (arr[1] * 255.0) as u8,
            b: (arr[2] * 255.0) as u8,
            a: (arr[3] * 255.0) as u8,
        }
    }
}

impl Into<[f32; 4]> for Colour {
    /// Converts the provided array of normalised floats to a `Colour` struct.
    fn into(self) -> [f32; 4] {
        [
            self.r as f32 / 255.0,
            self.g as f32 / 255.0,
            self.b as f32 / 255.0,
            self.a as f32 / 255.0,
        ]
    }
}