/// Contains a data buffer and depth buffer for rendering. The data buffer can contain anything,
/// but will commonly be used for the likes of colour. To render triangles to the `Framebuffer`,
/// use a `renderer::Renderer`.
pub struct Framebuffer<T> 
    where T: Clone,
{
    width: usize,
    height: usize,
    pub min_z: f32,
    pub max_z: f32,
    pub data_buf: Vec<T>,
    pub depth_buf: Vec<f32>,
}

impl <T> Framebuffer<T> 
    where T: Clone,
{
    /// Create a new `Framebuffer`. Fragments outside of the provided depth bounds will not be
    /// rendered.
    pub fn new(width: usize, height: usize, min_depth: f32, max_depth: f32, fill: T) -> Self {
        let size = width * height;
        let data_buf = vec![fill; size];

        Self {
            width,
            height,
            min_z: -max_depth,
            max_z: -min_depth,
            data_buf,
            depth_buf: vec![std::f32::NEG_INFINITY; size],
        }
    }

    /// If provided depth value is more than the value in the depth buffer at the specified
    /// coordinates, then it is written to the depth buffer and true is returned. Otherwise,
    /// false is returned.
    pub fn try_write_depth(&mut self, x: usize, y: usize, depth: f32) -> bool {
        let idx = x + y * self.width;
        if depth > self.depth_buf[idx] && depth >= self.min_z && depth <= self.max_z {
            self.depth_buf[idx] = depth;
            true
        } else {
            false
        }
    }

    /// Sets the colour values at the specified coordinates to match the provided colour
    pub fn write_data(&mut self, x: usize, y: usize, data: T) {
        let data_idx = x + y * self.width;
        self.data_buf[data_idx] = data;
    }

    /// Clear the colour buffer and frame buffer, filling the colour buffer
    /// with the provided colour values
    pub fn clear(&mut self, clear_value: T) {
        for value in self.data_buf.iter_mut() {
            *value = clear_value.clone();
        }
        for v in self.depth_buf.iter_mut() {
            *v = std::f32::NEG_INFINITY;
        }
    }

    /// Get the width of the frame buffer
    pub fn width(&self) -> usize {
        self.width
    }

    /// Get the height of the frame buffer
    pub fn height(&self) -> usize {
        self.height
    }
}

