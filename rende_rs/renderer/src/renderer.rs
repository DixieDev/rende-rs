use crate::framebuffer::Framebuffer;
use geo::{Triangle, Vector2, Vector3, Vertex};

/// Implementations of this trait can be provided when creating a `Renderer`. When the `Renderer` 
/// is used to render a mesh via `Renderer::render_triangles`, each fragment rendered for the 
/// triangle will pass through the implemented `apply` function.
pub trait FragmentShader<Fragment, Uniforms> {
    fn apply(&self, vertex: Vertex, fragment: &Fragment, uniforms: &Uniforms) -> Fragment;
}

impl<Fragment, Uniforms, FS> FragmentShader<Fragment, Uniforms> for FS
    where FS: Fn(Vertex, &Fragment, &Uniforms) -> Fragment
{
    fn apply(&self, vertex: Vertex, fragment: &Fragment, uniforms: &Uniforms) -> Fragment {
        self(vertex, fragment, uniforms)
    }
}

/// Implementations of this trait can be provided when creating a `Renderer`. When the `Renderer`
/// is used to render a mesh via `Renderer::render_triangles`, each vertex of each provided
/// triangle will pass through the implemented `apply` function before fragments are rendered.
pub trait VertexShader<Uniforms> {
    fn apply(&self, vertex: &Vertex, uniforms: &Uniforms) -> Vertex;
}

impl<Uniforms, VS> VertexShader<Uniforms> for VS 
    where VS: Fn(&Vertex, &Uniforms) -> Vertex
{
    fn apply(&self, vertex: &Vertex, uniforms: &Uniforms) -> Vertex {
        self(vertex, uniforms)
    }
}

/// A `Renderer` is used to render triangles to a `Framebuffer` by making use of the provided
/// vertex and fragment shaders. 
pub struct Renderer<'a, Uniforms, Fragment, VS, FS>
    where Fragment: Clone,
          VS: VertexShader<Uniforms>,
          FS: FragmentShader<Fragment, Uniforms>,
{
    vertex_shader: &'a VS,
    fragment_shader: &'a FS, 
    uniforms_phantom: std::marker::PhantomData<Uniforms>,
    fragment_phantom: std::marker::PhantomData<Fragment>,
}

impl<'a, Uniforms, Fragment, VS, FS> Renderer<'a, Uniforms, Fragment, VS, FS> 
    where Fragment: Clone,
          VS: VertexShader<Uniforms>,
          FS: FragmentShader<Fragment, Uniforms>,
{
    /// Create a `Renderer` by providing a `VertexShader` and `FragmentShader` implementation.
    pub fn new(vertex_shader: &'a VS, fragment_shader: &'a FS) -> Self{
        Self {
            vertex_shader,
            fragment_shader,
            uniforms_phantom: std::marker::PhantomData,
            fragment_phantom: std::marker::PhantomData,
        }
    }

    /// Render all the provided triangles to the provided framebuffer. The provided `uniforms` will 
    /// be passed to the `Renderer`'s shaders.
    pub fn render_triangles(&mut self, framebuffer: &mut Framebuffer<Fragment>, triangles: &[Triangle], uniforms: &Uniforms) { 
        for i in 0..triangles.len() {
            // Generate triangle by applying vertex shader
            let tri = Triangle::new([
                    self.vertex_shader.apply(&triangles[i].verts[0], uniforms), 
                    self.vertex_shader.apply(&triangles[i].verts[1], uniforms),
                    self.vertex_shader.apply(&triangles[i].verts[2], uniforms),
            ]);

            // Get bounds for triangle
            let min_y = f32::min(tri.verts[0].position.y, f32::min(tri.verts[1].position.y, tri.verts[2].position.y));
            let max_y = f32::max(tri.verts[0].position.y, f32::max(tri.verts[1].position.y, tri.verts[2].position.y));
            let min_x = f32::min(tri.verts[0].position.x, f32::min(tri.verts[1].position.x, tri.verts[2].position.x));
            let max_x = f32::max(tri.verts[0].position.x, f32::max(tri.verts[1].position.x, tri.verts[2].position.x));
            
            if min_y < framebuffer.height() as f32 && max_y >= 0.0 
                && min_x < framebuffer.width() as f32 && max_x >= 0.0 
            {
                self.render_triangle(
                    &tri,
                    framebuffer,
                    uniforms,
                    Bounds { min_x, max_x, min_y, max_y }
                );
            }
        }
    }

    /// Renders an individual triangle.
    #[inline]
    fn render_triangle(
        &mut self,
        tri: &Triangle,
        framebuffer: &mut Framebuffer<Fragment>,
        uniforms: &Uniforms,
        bounds: Bounds
    ) {
        // Clamp bounds to fit within framebuffer's bounds, and convert to usize
        let min_y = f32::max(0.0, bounds.min_y.floor()) as usize;
        let max_y = f32::min(framebuffer.height() as f32, bounds.max_y.ceil()) as usize;
        let min_x = f32::max(0.0, bounds.min_x.floor()) as usize;
        let max_x = f32::min(framebuffer.width() as f32, bounds.max_x.ceil()) as usize;

        // Extract vertex info into shorter names so the code isn't a mess later on
        let (pos0, pos1, pos2) = (tri.verts[0].position, tri.verts[1].position, tri.verts[2].position);
        let (norm0, norm1, norm2) = (tri.verts[0].normal, tri.verts[1].normal, tri.verts[2].normal);
        let (tex0, tex1, tex2) = (tri.verts[0].tex_coord, tri.verts[1].tex_coord, tri.verts[2].tex_coord);

        // Loop over bounding rect and render each fragment if valid
        for y in min_y..max_y {
            for x in min_x..max_x {
                // Get barycentric coords, and use them to check that 
                // the current fragment coords are within the triangle
                let Vector3 { x: t, y: u, z: v } = Self::barycentric_coords(x as f32, y as f32, pos0.x, pos0.y, pos1.x, pos1.y, pos2.x, pos2.y);
                if u + v <= 1.0 && u >= 0.0 && v >= 0.0 {
                    // Determine depth by interpolating z-coord across triangle verts
                    let z = pos0.z * t + pos1.z * u + pos2.z * v;

                    // If the new depth value overwrites the current one, it means we should write
                    // to the current fragment
                    let idx = x + y * framebuffer.width();
                    if z > framebuffer.depth_buf[idx] && z >= framebuffer.min_z && z <= framebuffer.max_z {
                        framebuffer.depth_buf[idx] = z;
                        // Interpolate normal across triangle verts
                        let norm_x = norm0.x * t + norm1.x * u + norm2.x * v;
                        let norm_y = norm0.y * t + norm1.y * u + norm2.y * v;
                        let norm_z = norm0.z * t + norm1.z * u + norm2.z * v;
                        
                        // Interpolate tex coords across triangle verts
                        let tex_x = tex0.x * t + tex1.x * u + tex2.x * v;
                        let tex_y = tex0.y * t + tex1.y * u + tex2.y * v;

                        let frag_vertex = Vertex {
                            position: Vector3::new(x as f32, y as f32, z),
                            normal: Vector3::new(norm_x, norm_y, norm_z),
                            tex_coord: Vector2::new(tex_x, tex_y),
                        };

                        // Run fragment shader and write output to data slice
                        let cur_fragment = &framebuffer.data_buf[idx];
                        let fragment = self.fragment_shader.apply(frag_vertex, cur_fragment, &uniforms);
                        framebuffer.data_buf[idx] = fragment;
                    }
                }
            }
        }
    }

    #[inline]
    fn barycentric_coords(
        x: f32, y: f32,
        x0: f32, y0: f32,
        x1: f32, y1: f32,
        x2: f32, y2: f32
    ) -> Vector3 {
        // Get edge vectors AB, AC, and PA as ints
        let abx = x1 - x0;
        let aby = y1 - y0;

        let acx = x2 - x0;
        let acy = y2 - y0;

        let pax = x0 - x;
        let pay = y0 - y;

        // Cross product vector (abx, acx,pax) against (aby, acy, pay)
        let cross_x = acx * pay - acy * pax;
        let cross_y = aby * pax - abx * pay;
        let cross_z = abx * acy - aby * acx;

        if cross_z == 0.0 {
            // If Z is 0 then the point or triangle is bad, so just return a coord
            // outside of the triangle
            Vector3::new(-1.0, -1.0, -1.0)
        } else {
            // Determine barycentric coords t, u, and v by scaling cross product such
            // that its Z component is 1
            let u = cross_x / cross_z;
            let v = cross_y / cross_z;
            let t = 1.0 - (u + v);

            Vector3::new(t, u, v)
        }
    }
}

struct Bounds {
    min_x: f32,
    max_x: f32,
    min_y: f32,
    max_y: f32,
}
