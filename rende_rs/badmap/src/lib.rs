use std::fs::File;
use std::io;
use std::io::{Write, Read, Seek, SeekFrom};
use std::path::Path;

const BMP_HEADER_SIZE: u32 = 14;
const DIB_HEADER_SIZE: u32 = 40;

const MAGIC_NUMBER: u16 = 19778;

#[repr(C, packed)]
struct FileHeader {
    magic_number: u16,
    size: u32,
    reserved: u32,
    offset: u32,
}

impl FileHeader {
    fn to_bytes(self) -> [u8; 14] {
        unsafe { std::mem::transmute(self) }
    }

    fn from_bytes(bytes: [u8; 14]) -> Self {
        unsafe { std::mem::transmute(bytes) }
    }
}

#[repr(C, packed)]
struct InfoHeader {
    size: u32,
    width: i32,
    height: i32,
    planes: u16,
    bit_count: u16,
    compression: u32,
    size_image: u32,
    x_pels_per_meter: i32,
    y_pels_per_meter: i32,
    clr_used: u32,
    clr_important: u32,
}

impl InfoHeader {
    fn to_bytes(self) -> [u8; 40] {
        unsafe { std::mem::transmute(self) }
    }

    fn from_bytes(bytes: [u8; 40]) -> Self {
        unsafe { std::mem::transmute(bytes) }
    }
}


pub struct Bitmap {
    bytes: Vec<u8>,
    width: i32,
    height: i32,
}

impl Bitmap {
    // TODO: Proper error handling
    pub fn load(path: &Path) -> io::Result<Self> {
        // Open file
        let mut file = File::open(path)?;

        // Read file header
        let mut file_header_bytes = [0; 14];
        assert_eq!(14, file.read(&mut file_header_bytes)?);
        let file_header = FileHeader::from_bytes(file_header_bytes);

        // Read dib header
        let mut dib_header_bytes = [0; 40];
        assert_eq!(40, file.read(&mut dib_header_bytes)?);
        let dib_header = InfoHeader::from_bytes(dib_header_bytes);

        // Only support 24 bit (RGB888) bit depth because lazy
        let bit_count = dib_header.bit_count;
        assert_eq!(24, bit_count, "Only uncompressed 24-bit Bitmaps are supported");

        // Seek to the specified offset
        file.seek(SeekFrom::Start(file_header.offset.into()))?;

        // Number of bytes per row should be rounded up to the nearest multiple of 4
        let uwidth = i32::abs(dib_header.width) as usize;
        let uheight = i32::abs(dib_header.height) as usize;
        let pixel_bytes_per_row = uwidth * 3;
        let row_len = ((pixel_bytes_per_row + 3) / 4) * 4;
        let row_padding = row_len - pixel_bytes_per_row;

        // Allocate memory for the pixel array
        let num_bytes = (pixel_bytes_per_row + row_padding) * uheight;
        let mut bytes = Vec::with_capacity(num_bytes);

        // Read pixel data from file
        unsafe { bytes.set_len(bytes.capacity()) };
        file.read_exact(&mut bytes)?;

        Ok(Self {
            width: dib_header.width,
            height: dib_header.height,
            bytes,
        })
    }

    pub fn new(width: i32, height: i32, rgba_pixel_data: &[u8]) -> Self {
        let uwidth = i32::abs(width) as usize;
        let uheight = i32::abs(height) as usize;

        // Number of bytes per row should be rounded up to the nearest multiple of 4
        let pixel_bytes_per_row = uwidth * 3;
        let row_len = ((pixel_bytes_per_row + 3) / 4) * 4;
        let row_padding = row_len - pixel_bytes_per_row;

        let pixel_array_size = row_len * uheight;

        let mut pixel_data = Vec::with_capacity(pixel_array_size);
        for y in 0..uheight {
            for x in 0..uwidth {
                // Push each pixel's RGB values
                let pixel_idx = (x + y * uwidth) * 4;
                pixel_data.push(rgba_pixel_data[pixel_idx + 2]);
                pixel_data.push(rgba_pixel_data[pixel_idx + 1]);
                pixel_data.push(rgba_pixel_data[pixel_idx + 0]);
            }
            // Pad the row to the nearest multiple of 4
            for _ in 0..row_padding {
                pixel_data.push(0);
            }
        }

        Self {
            width,
            height,
            bytes: pixel_data,
        }
    }

    fn generate_bmp_header(&self) -> [u8; 14] {
        FileHeader {
            magic_number: MAGIC_NUMBER,
            size: BMP_HEADER_SIZE + DIB_HEADER_SIZE + self.bytes.len() as u32,
            reserved: 0,
            offset: BMP_HEADER_SIZE + DIB_HEADER_SIZE,
        }.to_bytes()
    }

    fn generate_dib_header(&self) -> [u8; 40] {
        InfoHeader {
            size: DIB_HEADER_SIZE,
            width: self.width,
            height: self.height,
            planes: 1,
            bit_count: 24,
            compression: 0,
            size_image: 0,
            x_pels_per_meter: 0,
            y_pels_per_meter: 0,
            clr_used: 0,
            clr_important: 0,           
        }.to_bytes()
    }

    pub fn export(&self, path: &Path) -> Result<(), String> {
        // Generate bmp header
        let header = self.generate_bmp_header();

        // Generate dib header
        let dib_header = self.generate_dib_header();

        // Write data to file
        let mut f = File::create(path).map_err(|e| e.to_string())?;
        f.write_all(&header).map_err(|e| e.to_string())?;
        f.write_all(&dib_header).map_err(|e| e.to_string())?;
        f.write_all(&self.bytes).map_err(|e| e.to_string())?;

        Ok(())
    }

    pub fn width(&self) -> i32 {
        self.width
    }

    pub fn height(&self) -> i32 {
        self.height
    }

    pub fn bytes(&self) -> &[u8] {
        &self.bytes
    }
}