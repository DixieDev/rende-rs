use crate::framebuffer::Framebuffer;
use geo::{Triangle, Vector2, Vector3, Vertex};

pub trait FragmentShader<Fragment, Uniforms> {
    fn apply(&self, vertex: Vertex, fragment: &Fragment, uniforms: &Uniforms) -> Fragment;
}

impl<Fragment, Uniforms, FS> FragmentShader<Fragment, Uniforms> for FS
    where FS: Fn(Vertex, &Fragment, &Uniforms) -> Fragment + Send + Sync 
{
    fn apply(&self, vertex: Vertex, fragment: &Fragment, uniforms: &Uniforms) -> Fragment {
        self(vertex, fragment, uniforms)
    }
}

pub trait VertexShader<Uniforms> {
    fn apply(&self, vertex: &Vertex, uniforms: &Uniforms) -> Vertex;
}

impl<Uniforms, VS> VertexShader<Uniforms> for VS 
    where VS: Fn(&Vertex, &Uniforms) -> Vertex + Send + Sync 
{
    fn apply(&self, vertex: &Vertex, uniforms: &Uniforms) -> Vertex {
        self(vertex, uniforms)
    }
}

pub struct MeshInfo {
    pub num_triangles: usize,
}

pub struct Renderer<'a, Uniforms, Fragment, VS, FS>
    where Uniforms: Send + Sync,
          Fragment: Clone + Send + Sync,
          VS: VertexShader<Uniforms> + Send + Sync,
          FS: FragmentShader<Fragment, Uniforms> + Send + Sync,
{
    vertex_shader: &'a VS,
    fragment_shader: &'a FS, 
    tri_buffer: Vec<Triangle>,
    uniforms_phantom: std::marker::PhantomData<Uniforms>,
    fragment_phantom: std::marker::PhantomData<Fragment>,
}

impl<'a, Uniforms, Fragment, VS, FS> Renderer<'a, Uniforms, Fragment, VS, FS> 
    where Uniforms: Send + Sync,
          Fragment: Clone + Send + Sync,
          VS: VertexShader<Uniforms> + Send + Sync,
          FS: FragmentShader<Fragment, Uniforms> + Send + Sync,
{
    pub fn new(vertex_shader: &'a VS, fragment_shader: &'a FS) -> Self{
        Self {
            vertex_shader,
            fragment_shader,
            tri_buffer: Vec::new(),
            uniforms_phantom: std::marker::PhantomData,
            fragment_phantom: std::marker::PhantomData,
        }
    }

    pub fn register_mesh(&mut self, mesh_info: &MeshInfo) {
        if self.tri_buffer.capacity() < mesh_info.num_triangles {
            self.tri_buffer.reserve(mesh_info.num_triangles - self.tri_buffer.capacity());
        }
    }

    pub fn render_triangles(
        &mut self,
        framebuffer: &mut Framebuffer<Fragment>,
        triangles: &[Triangle],
        uniforms: &Uniforms,
        pool: &mut Pool,
    ) {
        assert!(
            triangles.len() <= self.tri_buffer.capacity(),
            "Too many triangles provided. Expected at most {}, found {}",
            self.tri_buffer.capacity(),
            triangles.len()
        );

        let thread_count = pool.thread_count() as usize;

        // TODO: Apply vertex shader in parallel
        // Apply vertex shader and determine vertical screen bounds for mesh
        let fb_height: f32 = framebuffer.height() as f32;
        let mut min_y = fb_height;
        let mut max_y = 0.0;
        self.tri_buffer.clear();
        for (idx, tri) in triangles.iter().enumerate() {
            self.tri_buffer.push(Triangle::new([
                    self.vertex_shader.apply(&tri.verts[0], uniforms), 
                    self.vertex_shader.apply(&tri.verts[1], uniforms),
                    self.vertex_shader.apply(&tri.verts[2], uniforms),
            ]));
            for vert in self.tri_buffer[idx].verts.iter() {
                min_y = f32::max(0.0, f32::min(min_y, vert.position.y));
                max_y = f32::min(fb_height, f32::max(max_y, vert.position.y));
            }
        }

        // Can exit early if none of the verts are within bounds
        if min_y > max_y {
            return;
        }

        // Divide bounding box height by thread count to determine independent sections
        let bb_height = f32::ceil(max_y - min_y) as usize;
        let section_height = {
            let mut section_height = bb_height / thread_count;
            if section_height == 0 {
                1
            } 
            else {
                let remainder = bb_height % section_height;
                if remainder != 0 {
                    section_height += 1;
                }
                section_height
            }
        };

        // Record indexes for triangles in each section
        let section_count = {
            let mut section_count = bb_height / section_height;
            if bb_height % section_height != 0 {
                section_count += 1;
            }
            section_count
        };

        let mut sections = Vec::with_capacity(section_count);
        for i in 0..section_count {
            let min_y = min_y + (section_height * i) as f32;
            let max_y = f32::min(max_y, min_y + section_height as f32);
            let mut section = Vec::with_capacity(self.tri_buffer.len());
            for (tri_idx, triangle) in self.tri_buffer.iter().enumerate() {
                let verts = &triangle.verts;
                let mut tri_min_y = verts[0].position.y;
                let mut tri_max_y = verts[0].position.y;
                for vert in verts[1..].iter() {
                    tri_min_y = f32::min(tri_min_y, vert.position.y);
                    tri_max_y = f32::max(tri_max_y, vert.position.y);
                }
                if tri_min_y <= max_y && tri_max_y >= min_y {
                    section.push(tri_idx);
                }
            }
            sections.push(section);
        }

        // Render each section in separate thread
        let fb_width = framebuffer.width();
        pool.scoped(|scope| {
            // Iterate over chunks of the framebuffer's colour and depth buffers. These chunks
            // should cover the bytes that each section cares about
            let chunks_start = min_y as usize * framebuffer.width();
            let chunks_end = max_y as usize * framebuffer.width();

            let chunk_size = section_height * framebuffer.width();
            let data_chunks =
                framebuffer.data_buf[chunks_start..chunks_end].chunks_mut(chunk_size);
            let depth_chunks =
                framebuffer.depth_buf[chunks_start..chunks_end].chunks_mut(chunk_size);

            for (section_idx, bundle) in data_chunks.zip(depth_chunks).enumerate() {
                // Break early if there are no more sections
                if section_idx >= sections.len() {
                    break;
                }
                let section = &sections[section_idx];

                let triangles = &self.tri_buffer;
                let fragment_shader = self.fragment_shader;
                let min_z = framebuffer.min_z;
                let max_z = framebuffer.max_z;
                scope.execute(move || {
                    // Determine start and end Y coord of current section
                    let section_start_y = min_y as usize + (section_idx * section_height);
                    let section_end_y =
                        usize::min(max_y as usize, section_start_y as usize + section_height);

                    let data_slice = bundle.0;
                    let depth_slice = bundle.1;

                    // Draw each triangle
                    for tri_idx in section.iter().cloned() {
                        let tri = &triangles[tri_idx];
                        Self::render_triangle(
                            fragment_shader,
                            data_slice,
                            depth_slice,
                            min_z,
                            max_z,
                            fb_width,
                            section_start_y,
                            section_end_y,
                            tri,
                            uniforms
                        )
                    }
                });
            }
        });
    }

    // TODO: Fewer parameters....
    pub fn render_triangle(
        fragment_shader: &FS,
        fragment_slice: &mut [Fragment],
        depth_slice: &mut [f32],
        min_z: f32,
        max_z: f32,
        fb_width: usize,
        section_start_y: usize,
        section_end_y: usize,
        triangle: &Triangle,
        uniforms: &Uniforms,
    ) {
        // Extract values into more concisely named vars
        let verts = triangle.verts;

        let pos0 = verts[0].position;
        let norm0 = verts[0].normal;
        let tex0 = verts[0].tex_coord;

        let pos1 = verts[1].position;
        let norm1 = verts[1].normal;
        let tex1 = verts[1].tex_coord;

        let pos2 = verts[2].position;
        let norm2 = verts[2].normal;
        let tex2 = verts[2].tex_coord;

        // Determine bounding box, clamped to the section dimensions
        let min_x = f32::min(pos0.x, f32::min(pos1.x, pos2.x));
        let min_x = f32::max(0.0, min_x) as usize;
        let min_x = usize::min(fb_width, min_x);

        let max_x = f32::max(pos0.x, f32::max(pos1.x, pos2.x));
        let max_x = f32::max(0.0, f32::min(fb_width as f32, max_x)) as usize;
        let max_x = usize::min(fb_width, max_x + 1); // TODO Check this +1 makes sense

        let min_y = f32::min(pos0.y, f32::min(pos1.y, pos2.y));
        let min_y = f32::max(section_start_y as f32, min_y) as usize;
        let min_y = usize::min(section_end_y, min_y);

        let max_y = f32::max(pos0.y, f32::max(pos1.y, pos2.y));
        let max_y = f32::max(section_start_y as f32, max_y) as usize;
        let max_y = usize::min(section_end_y, max_y + 1); // TODO Check this +1 makes sense

        // If there's nothing in bounds, return early
        if min_x >= max_x || min_y >= max_y {
            return;
        }

        // TODO: Enable and improve this?
        // Scuffed and disabled backface culling
        if norm0.z < 0.0 && norm1.z < 0.0 && norm2.z < 0.0 {
            //return;
        }

        // Loop over the pixels in the bounding box, attempting to render them if they are within the triangle
        for y in min_y..max_y {
            for x in min_x..max_x {
                // If the current pixel's barycentric coords are both positive and sum to 1.0 or
                // less than the current point is within the triangle and we should try to draw it
                let Vector3 { x: t, y: u, z: v } = triangle.barycentric_coords(x as f32, y as f32);
                if u + v <= 1.0 && u >= 0.0 && v >= 0.0 {
                    // Determine depth by interpolating Z across triangle verts
                    let z = pos0.z * t + pos1.z * u + pos2.z * v;

                    // If the new depth value overwrites the current one, it means we should write
                    // to the current fragment
                    let idx = x + (y - section_start_y) * fb_width;
                    let depth_updated = {
                        if z > depth_slice[idx] && z >= min_z && z <= max_z {
                            depth_slice[idx] = z;
                            true
                        } else {
                            false
                        }
                    };

                    // If we updated the depth buffer then we should also draw this pixel
                    if depth_updated {
                        // Interpolate normal across triangle verts
                        let norm_x = norm0.x * t + norm1.x * u + norm2.x * v;
                        let norm_y = norm0.y * t + norm1.y * u + norm2.y * v;
                        let norm_z = norm0.z * t + norm1.z * u + norm2.z * v;
                        
                        // Interpolate tex coords across triangle verts
                        let tex_x = tex0.x * t + tex1.x * u + tex2.x * v;
                        let tex_y = tex0.y * t + tex1.y * u + tex2.y * v;

                        let frag_vertex = Vertex {
                            position: Vector3::new(x as f32, y as f32, z),
                            normal: Vector3::new(norm_x, norm_y, norm_z),
                            tex_coord: Vector2::new(tex_x, tex_y),
                        };

                        // Run fragment shader and write output to data slice
                        let cur_fragment = &fragment_slice[idx];
                        let fragment = fragment_shader.apply(frag_vertex, cur_fragment, &uniforms);
                        fragment_slice[idx] = fragment;
                    }
                }
            }
        }
    }
}
