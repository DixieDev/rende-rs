use std::convert::{From, Into};
use std::ops::{Add, AddAssign, Div, Mul, MulAssign, Neg, Sub, SubAssign};

pub fn v(x: f32, y: f32, z: f32) -> Vector3 {
    Vector3::new(x, y, z)
}

#[derive(Clone, Copy, PartialEq)]
pub struct Vector2 {
    pub x: f32,
    pub y: f32,
}

impl Vector2 {
    pub fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }
}

#[derive(Clone, Copy, PartialEq)]
pub struct Vector3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Vector3 {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        Self { x, y, z }
    }

    pub fn dot(v1: Self, v2: Self) -> f32 {
        v1.x * v2.x + v1.y * v2.y + v1.z * v2.z
    }

    pub fn cross(v1: Self, v2: Self) -> Self {
        Self {
            x: v1.y * v2.z - v2.y * v1.z,
            y: v2.x * v1.z - v1.x * v2.z,
            z: v1.x * v2.y - v2.x * v1.y,
        }
    }

    pub fn length_squared(self) -> f32 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    pub fn length(self) -> f32 {
        f32::sqrt(self.x * self.x + self.y * self.y + self.z * self.z)
    }

    pub fn normalized(self) -> Self {
        let len = self.length();
        if len == 0.0 {
            Self {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            }
        } else {
            Self {
                x: self.x / len,
                y: self.y / len,
                z: self.z / len,
            }
        }
    }
}

impl Add<Vector3> for Vector3 {
    type Output = Self;
    fn add(self, v: Self) -> Self::Output {
        Self::Output {
            x: self.x + v.x,
            y: self.y + v.y,
            z: self.z + v.z,
        }
    }
}

impl AddAssign<Vector3> for Vector3 {
    fn add_assign(&mut self, v: Self) {
        self.x += v.x;
        self.y += v.y;
        self.z += v.z;
    }
}

impl Sub<Vector3> for Vector3 {
    type Output = Self;
    fn sub(self, v: Self) -> Self::Output {
        Self::Output {
            x: self.x - v.x,
            y: self.y - v.y,
            z: self.z - v.z,
        }
    }
}

impl SubAssign<Vector3> for Vector3 {
    fn sub_assign(&mut self, v: Self) {
        self.x -= v.x;
        self.y -= v.y;
        self.z -= v.z;
    }
}

impl Mul<f32> for Vector3 {
    type Output = Self;
    fn mul(self, v: f32) -> Self::Output {
        Self::Output {
            x: self.x * v,
            y: self.y * v,
            z: self.z * v,
        }
    }
}

impl Mul<Vector3> for f32 {
    type Output = Vector3;
    fn mul(self, v: Vector3) -> Self::Output {
        Self::Output {
            x: v.x * self,
            y: v.y * self,
            z: v.z * self,
        }
    }
}

impl MulAssign<f32> for Vector3 {
    fn mul_assign(&mut self, v: f32) {
        self.x *= v;
        self.y *= v;
        self.z *= v;
    }
}

impl Div<f32> for Vector3 {
    type Output = Self;
    fn div(self, v: f32) -> Self::Output {
        Self::Output {
            x: self.x / v,
            y: self.y / v,
            z: self.z / v,
        }
    }
}

impl Neg for Vector3 {
    type Output = Self;
    fn neg(self) -> Self::Output {
        Self::Output {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl Into<[f32; 3]> for Vector3 {
    fn into(self) -> [f32; 3] {
        [self.x, self.y, self.z]
    }
}

impl Into<[i64; 3]> for Vector3 {
    fn into(self) -> [i64; 3] {
        [self.x as i64, self.y as i64, self.z as i64]
    }
}

impl From<[f32; 3]> for Vector3 {
    fn from(arr: [f32; 3]) -> Self {
        Self {
            x: arr[0],
            y: arr[1],
            z: arr[2],
        }
    }
}

impl From<[i64; 3]> for Vector3 {
    fn from(arr: [i64; 3]) -> Self {
        Self {
            x: arr[0] as f32,
            y: arr[1] as f32,
            z: arr[2] as f32,
        }
    }
}
