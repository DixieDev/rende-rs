mod vector; 

pub use crate::vector::{v, Vector2, Vector3};

use std::ops::Mul;

#[derive(Clone)]
pub struct Transform {
    pub position: Vector3,
    pub rotation: Vector3,
    pub scale: Vector3,
}

impl Transform {
    pub fn apply_to_vertex(&self, vert: Vertex) -> Vertex {
        let mut result = vert;
        result.position.x *= self.scale.x;
        result.position.y *= self.scale.y;
        result.position.z *= self.scale.z;
        let rot_mat = self.rotation_matrix_xyz();
        result.position = rot_mat * result.position;
        result.normal = rot_mat * result.normal;
        result.position += self.position;
        result
    }

    pub fn translate(&mut self, translation: Vector3) {
        self.position += translation;
    }

    pub fn rotate(&mut self, rotation: Vector3) {
        self.rotation += rotation;
    }

    pub fn scale(&mut self, scale: Vector3) {
        self.scale.x *= scale.x;
        self.scale.y *= scale.y;
        self.scale.z *= scale.z;
    }

    pub fn rotation_matrix_xyz(&self) -> Mat3 {
        let x_rot = Mat3::x_rotation(self.rotation.x);
        let y_rot = Mat3::y_rotation(self.rotation.y);
        let z_rot = Mat3::z_rotation(self.rotation.z);
        x_rot * y_rot * z_rot
    }
}

#[derive(Clone, Copy, PartialEq)]
pub struct Mat3 {
    values: [f32; 9],
}

impl Mat3 {
    pub fn new() -> Self {
        Self { values: [0.0; 9] }
    }

    pub fn identity() -> Self {
        Self {
            values: [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0],
        }
    }

    pub fn x_rotation(rot: f32) -> Self {
        Self {
            values: [
                1.0,
                0.0,
                0.0,
                0.0,
                f32::cos(rot),
                -f32::sin(rot),
                0.0,
                f32::sin(rot),
                f32::cos(rot),
            ],
        }
    }

    pub fn y_rotation(rot: f32) -> Self {
        Self {
            values: [
                f32::cos(rot),
                0.0,
                f32::sin(rot),
                0.0,
                1.0,
                0.0,
                -f32::sin(rot),
                0.0,
                f32::cos(rot),
            ],
        }
    }

    pub fn z_rotation(rot: f32) -> Self {
        Self {
            values: [
                f32::cos(rot),
                -f32::sin(rot),
                0.0,
                f32::sin(rot),
                f32::cos(rot),
                0.0,
                0.0,
                0.0,
                1.0,
            ],
        }
    }

    pub fn transponse(&self) -> Mat3 {
        Mat3 {
            values: [
                self.values[0], self.values[3], self.values[6],
                self.values[1], self.values[4], self.values[7],
                self.values[2], self.values[5], self.values[8],
            ]
        }
    }

    pub fn value(&self, x: usize, y: usize) -> f32 {
        assert!(x < 3);
        assert!(y < 3);
        self.values[x + y * 3]
    }

    pub fn set_value(&mut self, x: usize, y: usize, v: f32) {
        assert!(x < 3);
        assert!(y < 3);
        self.values[x + y * 3] = v;
    }
}

impl Mul<Vector3> for Mat3 {
    type Output = Vector3;

    fn mul(self, v: Vector3) -> Self::Output {
        Self::Output {
            x: v.x * self.values[0] + v.y * self.values[1] + v.z * self.values[2],
            y: v.x * self.values[3] + v.y * self.values[4] + v.z * self.values[5],
            z: v.x * self.values[6] + v.y * self.values[7] + v.z * self.values[8],
        }
    }
}

impl Mul<Mat3> for Mat3 {
    type Output = Self;

    fn mul(self, m: Self) -> Self::Output {
        // TODO: Optimise maybe?
        Self::Output {
            values: [
                self.values[0] * m.values[0]
                    + self.values[1] * m.values[3]
                    + self.values[2] * m.values[6],
                self.values[0] * m.values[1]
                    + self.values[1] * m.values[4]
                    + self.values[2] * m.values[7],
                self.values[0] * m.values[2]
                    + self.values[1] * m.values[5]
                    + self.values[2] * m.values[8],
                self.values[3] * m.values[0]
                    + self.values[4] * m.values[3]
                    + self.values[5] * m.values[6],
                self.values[3] * m.values[1]
                    + self.values[4] * m.values[4]
                    + self.values[5] * m.values[7],
                self.values[3] * m.values[2]
                    + self.values[4] * m.values[5]
                    + self.values[5] * m.values[8],
                self.values[6] * m.values[0]
                    + self.values[7] * m.values[3]
                    + self.values[8] * m.values[6],
                self.values[6] * m.values[1]
                    + self.values[7] * m.values[4]
                    + self.values[8] * m.values[7],
                self.values[6] * m.values[2]
                    + self.values[7] * m.values[5]
                    + self.values[8] * m.values[8],
            ],
        }
    }
}


#[derive(Copy, Clone)]
pub struct Vertex {
    pub position: Vector3,
    pub tex_coord: Vector2,
    pub normal: Vector3,
}

impl Vertex {
    pub fn new(position: Vector3, tex_coord: Vector2, normal: Vector3) -> Self {
        Self {
            position,
            tex_coord,
            normal,
        }
    }
}

#[derive(Clone)]
pub struct Triangle {
    pub verts: [Vertex; 3],
}

impl Triangle {
    pub fn new(verts: [Vertex; 3]) -> Self {
        Self { verts }
    }

    pub fn rotate(&mut self, x: f32, y: f32, z: f32) {
        let x_rot = if x == 0.0 {
            Mat3::identity()
        } else {
            Mat3::x_rotation(x)
        };
        let y_rot = if y == 0.0 {
            Mat3::identity()
        } else {
            Mat3::y_rotation(y)
        };
        let z_rot = if z == 0.0 {
            Mat3::identity()
        } else {
            Mat3::z_rotation(z)
        };
        for i in 0..3 {
            self.verts[i].position = x_rot * self.verts[i].position;
            self.verts[i].position = y_rot * self.verts[i].position;
            self.verts[i].position = z_rot * self.verts[i].position;

            self.verts[i].normal = x_rot * self.verts[i].normal;
            self.verts[i].normal = y_rot * self.verts[i].normal;
            self.verts[i].normal = z_rot * self.verts[i].normal;
        }
    }

    pub fn scale(&mut self, scale: f32) {
        for i in 0..3 {
            self.verts[i].position.x *= scale;
            self.verts[i].position.y *= scale;
            self.verts[i].position.z *= scale;
        }
    }

    pub fn translate(&mut self, translation: Vector3) {
        for i in 0..3 {
            self.verts[i].position = self.verts[i].position + translation;
        }
    }

    pub fn barycentric_coords(&self, x: f32, y: f32) -> Vector3 {
        // Extract coords to vars with shorter names
        let x0 = self.verts[0].position.x;
        let y0 = self.verts[0].position.y;

        let x1 = self.verts[1].position.x;
        let y1 = self.verts[1].position.y;

        let x2 = self.verts[2].position.x;
        let y2 = self.verts[2].position.y;

        // Get edge vectors AB, AC, and PA as ints
        let abx = x1 - x0;
        let aby = y1 - y0;

        let acx = x2 - x0;
        let acy = y2 - y0;

        let pax = x0 - x;
        let pay = y0 - y;

        // Cross product vector (abx, acx,pax) against (aby, acy, pay)
        let cross_x = acx * pay - acy * pax;
        let cross_y = aby * pax - abx * pay;
        let cross_z = abx * acy - aby * acx;

        // If Z is 0 then the point or triangle is bad, so just return a coord
        // outside of the triangle
        if cross_z == 0.0 {
            return Vector3::new(-1.0, -1.0, -1.0);
        }

        // Determine barycentric coords t, u, and v by scaling cross product such
        // that its Z component is 1
        let u = cross_x / cross_z;
        let v = cross_y / cross_z;
        let t = 1.0 - (u + v);

        Vector3::new(t, u, v)
    }
}
