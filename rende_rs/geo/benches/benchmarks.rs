use geo::{Vector2, Vector3, Vertex, Transform, Triangle};
use bencher::{benchmark_group, benchmark_main, Bencher};

fn make_test_triangle() -> Triangle {
    Triangle::new([
        Vertex {
            position: Vector3::new(0.0, 10.0, 0.0),
            tex_coord: Vector2::new(0.0, 0.0),
            normal: Vector3::new(0.0, 0.0, -1.0),
        },
        Vertex {
            position: Vector3::new(10.0, 0.0, 0.0),
            tex_coord: Vector2::new(1.0, 1.0),
            normal: Vector3::new(0.0, 0.0, -1.0),
        },
        Vertex {
            position: Vector3::new(0.0, 0.0, 0.0),
            tex_coord: Vector2::new(0.0, 1.0),
            normal: Vector3::new(0.0, 0.0, -1.0),
        },
    ])
}


fn apply_transform_to_vertex(b: &mut Bencher) {
    let vert = make_test_triangle().verts[0];
    let transform = Transform {
        position: Vector3::new(15.4, 10.1, 17.2),
        rotation: Vector3::new(0.314, 0.18, 0.06),
        scale: Vector3::new(2.0, 3.0, 1.3),
    };

    b.iter(|| transform.apply_to_vertex(vert));
}

fn barycentric_coordinates(b: &mut Bencher) {
    let bary_x = 5.0;
    let bary_y = 5.0;
    let tri = make_test_triangle();

    b.iter(|| tri.barycentric_coords(bary_x, bary_y));
}

benchmark_group!(
    benches,
    apply_transform_to_vertex,
    barycentric_coordinates
);

benchmark_main!(benches);
