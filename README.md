# What is rende-rs?

This project is my exploration of low-level computer graphics, without having to deal with all the crazy stuff involved with interfacing directly with graphics hardware. As this is mostly a 'self-learning' project, I will be avoiding the use of libraries to the point of implementing rather loosely connected functionality myself. I've only just started the project, and this has involved writing a Bitmap encoder solely so I can actually see the output of my software renderer!

Eventually, I would like to have a few things implemented here: 
 - A 3D renderer suitable for real-time usage (i.e. games!)
   - Progress is being made on this goal currently!
 - A 3D renderer built around ray-tracing for the highest quality renders
   - This goal has been pushed to the side until further notice; the real-time renderer is the focus for now.

I don't know a tremendous amount about what's involved maths-wise but I'm hoping that will improve as I progress through this project, and that newfound knowledge will encourage me to pursue more interesting features!

# Parallelism

rende-rs parallelises the rendering of triangles by batching them into sections of screen-space. This can be done without any duplication of the framebuffer, and makes it easy to scale to match the number of cores available on the host machine. 

The downside of this method is that it doesn't account for triangle density, meaning if one section contains three times as many triangles as the others then the load on the core that processes that section will be completely out of balance with the cores handling other sections. 

Density issues are somewhat mitigated by limiting the processed screen-space to the bounds of the mesh to be rendered before dividing it into sections. This way the worst case where there are no triangles in one section of the screen is avoided; although it still suffers from any imbalance in triangle density within the mesh's bounds.

Prior to this approach, parallelism was implemented over the rasterisation step. When rendering triangles, each pixel is independent of the others making what appears to be an embarassingly parallel problem. 

Splitting threads on batches of pixels worked exceedingly well for situations were a single triangle covers a high number of pixels, but as the amount of triangles per pixel increases, the overhead in calculating the distribution of data, as well as dispatching and joining threads increasingly outweighs any benefits gained from parallelising the process. 

# Coming Soon (tm)

The following features are planned as the next to be added, although the order is currently up in the air:
 - Various shader improvements
   - Allow flexible inputs and outputs for vertex shaders
   - Interpolate vertex shader outputs and provide as input to fragment shader
   - Some means of accessing textures (although the existing uniforms field might work fine for that)
 - A small game, making use of the SDL integration
   - Many improvements to the geo crate will likely be made as a part of this

# Renders

![Young Link Wireframe](screenshots/link_wireframe.bmp)

![Young Link Shaded (no depth testing)](screenshots/link_shaded_no_depthtest.bmp)

![Toad Shaded (depth testing)](screenshots/toad_flat.bmp)

![Toad Shaded With Interpolated Normals](screenshots/toad_smooth.bmp)

![Dragon With Many Triangles](screenshots/chorogon.bmp)

Gossip Stone with texture mapping and an outline effect implemented through shaders
![Textured Gossip Stone With Outline Shader](screenshots/gossip_stone_fragment_shader.png)

Since you can have any type represent a fragment, outputting ASCII is pretty straight forward (see the examples for more!)
![ASCII Gossip Stone](screenshots/ascii_gossip_stone.png)

